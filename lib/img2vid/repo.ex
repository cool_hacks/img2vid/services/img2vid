defmodule Img2vid.Repo do
  use Ecto.Repo,
    otp_app: :img2vid,
    adapter: Ecto.Adapters.Postgres
end
