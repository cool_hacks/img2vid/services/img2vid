defmodule Img2vid.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      #Img2vid.Repo, # NOTE: Don't need ecto
      # Start the Telemetry supervisor
      Img2vidWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Img2vid.PubSub},
      # Start the Endpoint (http/https)
      Img2vidWeb.Endpoint
      # Start a worker by calling: Img2vid.Worker.start_link(arg)
      # {Img2vid.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Img2vid.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    Img2vidWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
