defmodule Img2vidWeb.PageController do
  use Img2vidWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
