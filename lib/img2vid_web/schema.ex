defmodule Img2vidWeb.Schema do
  use Absinthe.Schema
  import_types Absinthe.Plug.Types

  alias Img2vidWeb.Resolver

  query do
  end

	mutation do
    @desc "Upload images & convert them to video & get result back"    
    field :upload, :string do
			arg :files, non_null(list_of(non_null(:upload)))

      resolve &Resolver.upload/3
		end
	end
end
