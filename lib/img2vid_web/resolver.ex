defmodule Img2vidWeb.Resolver do
  require Logger
  
  def all_videos(_root, _args, _info) do
    {:ok, [%{url: "https://google.com"}, %{url: "localhost"}]}
  end

  def create_session(_root, args, _info) do
    #case News.create_link(args) do
    #		{:ok, link} ->
    IO.inspect args
    {:ok, %{id: 1, name: args.name}}
    #	_error ->
    #			{:error, "could not create link"}
    #end
  end

  def upload(_root, args, _info) do
    args.files
    |> Enum.filter(& &1.content_type == "image/png")
    |> IO.inspect()
    |> img2vid()

    #Process.sleep(100)

    #IO.inspect args.files # this is a `%Plug.Upload{}` struct.

    #{:ok, "success"}
  end

  # cat *.png | ffmpeg -framerate 1 -f image2pipe -i - output.webm
  def img2vid(images) do
    File.mkdir("/tmp/img2vid")

    images = images
             |> Enum.map(& File.read!(&1.path))
             |> Enum.join()

    out_filename = "/tmp/img2vid/" <> UUID.uuid4() <> ".webm"
    
    case Rambo.run("ffmpeg",
      [
        "-framerate", "1",
        "-f", "image2pipe",
        "-i", "-",
        out_filename
      ],
      in: images
    ) do
      {:ok, %Rambo{
        err: _err,
        out: _out,
        status: 0
      }} ->
        video_bin = File.read!(out_filename)
        File.rm!(out_filename)
        video_data_url = "data:image/webm;base64,#{Base.encode64(video_bin)}"
        {:ok, video_data_url}
      
      {:error, %Rambo{err: err, out: _out, status: 1}} ->
        Logger.error("Failed to generate video: #{inspect(err)}")
        {:error, "Failed to generare video."}
    end
  end
end
