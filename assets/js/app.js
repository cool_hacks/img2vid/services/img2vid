// We import the CSS which is extracted to its own file by esbuild.
// Remove this line if you add a your own CSS build pipeline (e.g postcss).
import "../css/app.css"

// If you want to use Phoenix channels, run `mix help phx.gen.channel`
// to get started and then uncomment the line below.
// import "./user_socket.js"

// You can include dependencies in two ways.
//
// The simplest option is to put them in assets/vendor and
// import them using relative paths:
//
//     import "../vendor/some-package.js"
//
// Alternatively, you can `npm install some-package --prefix assets` and import
// them using a path starting with the package name:
//
//     import "some-package"
//

// Include phoenix_html to handle method=PUT/DELETE in forms and buttons.
import "phoenix_html"
// Establish Phoenix Socket and LiveView configuration.
import {Socket} from "phoenix"
import {LiveSocket} from "phoenix_live_view"
import topbar from "../vendor/topbar"

let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content")
let liveSocket = new LiveSocket("/live", Socket, {params: {_csrf_token: csrfToken}})

// Show progress bar on live navigation and form submits
topbar.config({barColors: {0: "#29d"}, shadowColor: "rgba(0, 0, 0, .3)"})
window.addEventListener("phx:page-loading-start", info => topbar.show())
window.addEventListener("phx:page-loading-stop", info => topbar.hide())

// connect if there are any LiveViews on the page
liveSocket.connect()

// expose liveSocket on window for web console debug logs and latency simulation:
// >> liveSocket.enableDebug()
// >> liveSocket.enableLatencySim(1000)  // enabled for duration of browser session
// >> liveSocket.disableLatencySim()
window.liveSocket = liveSocket

var formChange = function(event) {
  var previews = document.getElementById('previews');

  // remove children
  for (let i = 0; i < previews.childNodes.length; i++) {
    previews.removeChild(previews.childNodes[i])
  }

  // add new children
  for (let i = 0; i < event.target.files.length; i++) {
    var output = document.createElement("IMG");

    output.src = URL.createObjectURL(event.target.files[i]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }

    previews.appendChild(output);
  }
};

window.formChange = formChange;

var formSubmit = function(event) {
  window.mutationRequest()

  return false
}

window.formSubmit = formSubmit;

var mutationRequest = function() {
  var input = document.querySelector('input[type="file"]')
  var files = input.files;
  
  var fileNames = []
  for (let i = 0; i < files.length; i++) {
    fileNames.push(files[i].name);
  }
  window.fileNames = fileNames;

  var data = new FormData()
  // TODO: we can split parameters and query, but let's not worry about it for now!
  query = `mutation { upload(files: ${JSON.stringify(fileNames)})}`
  // END TODO

  // TODO: here's more proper way, but whatever
  //data.append('query', 'mutation { upload(files: ["a"])}')
  //data.append('query', 'mutation ($files: [Upload]) { upload(files: $files)}')
  //data.append('$files', fileNames)
  data.append('query', query);
  for (let i = 0; i < files.length; i++) {
    data.append(files[i].name, input.files[i])
  }

  //data.append('a', input.files[0])

  fetch('/graphql', {
    method: 'POST',
    body: data
  }).then(async (data) => {
    // Console log our return data
    var body = await data.json();
    var video_data = body.data.upload;

    // if we had video source before, clean it up:
    var video_display = document.getElementById("video_display")
    if (video_display) {
      video_display.remove();
    }

    var video_display = document.createElement("VIDEO");
    // id="video_display" width="320" height="240" controls
    video_display.id = "video_display"
    video_display.width = "320"
    video_display.height = "320"
    video_display.controls = "true"

    var video_source = document.createElement("SOURCE");
    video_source.src = video_data;
    video_source.type = "video/webm"
    video_source.id = "video_source"
    video_display.appendChild(video_source);

    var video = document.getElementById('video');
    video.appendChild(video_display);
  });
}

window.mutationRequest = mutationRequest;
